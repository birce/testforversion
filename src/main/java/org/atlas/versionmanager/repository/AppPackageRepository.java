package org.atlas.versionmanager.repository;

import org.atlas.versionmanager.model.AppPackage;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface AppPackageRepository extends CrudRepository<AppPackage, String> {
    Optional<AppPackage> findOneByPackageName(String packageName);
}

package org.atlas.versionmanager.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter @Setter
public class AppPackage {

    @Id
    String packageName;
    String version;
}

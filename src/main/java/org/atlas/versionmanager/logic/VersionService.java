package org.atlas.versionmanager.logic;

public class VersionService {

    public static String increaseVersion(String version){
        String[] versionParts = version.split("\\.");
        String majorPart = versionParts[0];
        String minorPart = versionParts[1];

        Integer majorPartInt = Integer.parseInt(majorPart);
        Integer minorPartInt = Integer.parseInt(minorPart);

        minorPartInt++;

        if(minorPartInt == 10){
            majorPartInt++;
            minorPartInt = 0;
        }

        return majorPartInt.toString() + "." + minorPartInt.toString();
    }

    public static String decreaseVersion(String version){
        String[] versionParts = version.split("\\.");
        String majorPart = versionParts[0];
        String minorPart = versionParts[1];

        Integer majorPartInt = Integer.parseInt(majorPart);
        Integer minorPartInt = Integer.parseInt(minorPart);

        minorPartInt--;

        if(minorPartInt == -1){
            majorPartInt--;
            minorPartInt = 9;
        }

        return majorPartInt.toString() + "." + minorPartInt.toString();
    }
}

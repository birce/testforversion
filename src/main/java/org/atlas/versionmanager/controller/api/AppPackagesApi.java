package org.atlas.versionmanager.controller.api;

import org.atlas.versionmanager.logic.VersionService;
import org.atlas.versionmanager.model.AppPackage;
import org.atlas.versionmanager.repository.AppPackageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/apppackages")
public class AppPackagesApi {

    @Autowired
    AppPackageRepository appPackageRepository;

    @GetMapping
    public List<AppPackage> getAll(){
        return (List<AppPackage>) appPackageRepository.findAll();
    }

    @GetMapping("/{packagename}")
    public String getByPackageName(@PathVariable("packagename") String packageName){
        AppPackage appPackage = appPackageRepository.findOneByPackageName(packageName).orElse(null);
        return appPackage != null ? appPackage.getVersion() : "500:Package can not be found!!!";
    }

    @PostMapping
    public AppPackage create(@RequestBody AppPackage appPackage){
        return appPackageRepository.save(appPackage);
    }

    @GetMapping("/{packagename}/increaseversion")
    public String increaseVersion(@PathVariable("packagename") String packageName){
        AppPackage appPackage = appPackageRepository.findOneByPackageName(packageName).orElse(null);

        if(appPackage == null){ return "500:Package can not be found!!!"; }

        appPackage.setVersion(VersionService.increaseVersion(appPackage.getVersion()));
        return appPackageRepository.save(appPackage).getVersion();
    }

    @GetMapping("/{packagename}/decreaseversion")
    public String decreaseVersion(@PathVariable("packagename") String packageName, Model model) {
        AppPackage appPackage = appPackageRepository.findOneByPackageName(packageName).orElse(null);

        if(appPackage == null){ return "500:Package can not be found!!!"; }

        appPackage.setVersion(VersionService.decreaseVersion(appPackage.getVersion()));
        return appPackageRepository.save(appPackage).getVersion();
    }

}


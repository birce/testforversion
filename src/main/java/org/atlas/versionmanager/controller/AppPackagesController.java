package org.atlas.versionmanager.controller;

import org.atlas.versionmanager.controller.api.AppPackagesApi;
import org.atlas.versionmanager.logic.VersionService;
import org.atlas.versionmanager.model.AppPackage;
import org.atlas.versionmanager.repository.AppPackageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;


@Controller
@RequestMapping("/apppackages")
public class AppPackagesController {

    @Autowired
    AppPackageRepository appPackageRepository;

    @GetMapping("addpackage")
    public String showAddPackageForm(AppPackage appPackage) {
        return "add-apppackages";
    }

    @GetMapping
    public String getAll(Model model){
        model.addAttribute("apppackages", appPackageRepository.findAll());
        return "index";
    }

    @PostMapping("/add")
    public String addPackage(@Valid AppPackage appPackage, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "add-apppackages";
        }

        appPackageRepository.save(appPackage);
        return "redirect:/apppackages";
    }

    @GetMapping("/{packagename}/increaseversion")
    public String increaseVersion(@PathVariable("packagename") String packageName, Model model) {
        AppPackage appPackage = appPackageRepository.findOneByPackageName(packageName).orElse(null);

        appPackage.setVersion(VersionService.increaseVersion(appPackage.getVersion()));
        appPackageRepository.save(appPackage);

        model.addAttribute("apppackages" , appPackageRepository.findAll());
        return "index";
    }

    @GetMapping("/{packagename}/decreaseversion")
    public String decreaseVersion(@PathVariable("packagename") String packageName, Model model) {
        AppPackage appPackage = appPackageRepository.findOneByPackageName(packageName).orElse(null);

        appPackage.setVersion(VersionService.decreaseVersion(appPackage.getVersion()));
        appPackageRepository.save(appPackage);

        model.addAttribute("apppackages" , appPackageRepository.findAll());
        return "index";
    }

    @GetMapping("/{packagename}/delete")
    public String deletePackage(@PathVariable("packagename") String packageName, Model model) {
        Optional<AppPackage> appPackage = appPackageRepository.findOneByPackageName(packageName);
        appPackageRepository.delete(appPackage.get());
        model.addAttribute("apppackages" , appPackageRepository.findAll());
        return "redirect:/apppackages";

    }
}
